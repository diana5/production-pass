package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Notas;

public interface NotasRepository extends JpaRepository<Notas,Integer> {

	
	//imporante esto de LIST <Objeto>
	List<Notas> findByUsuarioId(Integer id);

}

package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.util.MultiValueMap;

import com.example.demo.model.Imagen;

public interface ImagenRepository extends JpaRepository<Imagen,Integer> {


}

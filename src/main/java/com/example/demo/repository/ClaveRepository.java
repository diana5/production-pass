package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Claves;

public interface ClaveRepository extends JpaRepository<Claves,Integer>{

	List<Claves> findByUsuarioId(Integer id);


}

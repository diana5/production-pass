package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.UsuarioRol;

public interface UsuarioRolRepository extends JpaRepository<UsuarioRol,Integer>{

}

package com.example.demo.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Claves;
import com.example.demo.repository.ClaveRepository;


@RestController
@RequestMapping("V1/clave")

public class ClaveController {
	@Autowired
	ClaveRepository claveRepository;

	// listar todas las cajas
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Object> listarclaves() {

		return new ResponseEntity<Object>(claveRepository.findAll(), HttpStatus.OK);
	}

	// listar una caja
	@RequestMapping(method = RequestMethod.GET, path = "/{id}")
	public ResponseEntity<Object> listarunaclave(@PathVariable(value = "id") Integer id) {
		return new ResponseEntity<Object>(claveRepository.findById(id), HttpStatus.OK);
	}

	// Editar

	@RequestMapping(method = RequestMethod.PUT, path = "/{id}")
	public ResponseEntity<Claves> editarclave(@PathVariable(value = "id") Integer id,
			@Valid @RequestBody Claves detalleclave) {

		Optional<Claves>clave = claveRepository.findById(id);
		if (!clave.isPresent())
			return new ResponseEntity<Claves>(new Claves(), HttpStatus.NO_CONTENT);

		clave.get().setApellido(detalleclave.getApellido());
		clave.get().setEmail(detalleclave.getEmail());
		clave.get().setFecha(detalleclave.getFecha());
		clave.get().setId(detalleclave.getId());
		clave.get().setName(detalleclave.getName());
		clave.get().setNotas(detalleclave.getNotas());
		clave.get().setClave(detalleclave.getClave());
		claveRepository.save(clave.get());
		
		return new ResponseEntity<Claves>(clave.get(), HttpStatus.OK);

	}

	@RequestMapping(method = RequestMethod.DELETE, path = "/{id}")
	public ResponseEntity<Void> eliminarClave(@PathVariable("id") Integer id) {
		claveRepository.deleteById(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
	
	//guardar
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Object> crearClave(@Valid @RequestBody Claves clave) {
		claveRepository.save(clave);
		return new ResponseEntity<Object>(clave, HttpStatus.CREATED);
	}

}

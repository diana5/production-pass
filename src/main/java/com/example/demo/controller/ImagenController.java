package com.example.demo.controller;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.model.Imagen;
import com.example.demo.model.Usuario;
import com.example.demo.repository.ImagenRepository;
import com.example.demo.repository.UsuarioRepository;

@RestController
@RequestMapping("V1/img")

public class ImagenController {
	@Autowired
	ImagenRepository imagenRepository;
	
//obtener imagen por id 	
	
	
	@RequestMapping(method= RequestMethod.GET, path="/{id}")
	public  ResponseEntity<Object>listarundocumento(@PathVariable("id") int id){
	return new ResponseEntity<Object>(imagenRepository.findById(id), HttpStatus.OK);	
	}	
	

	
	
}

package com.example.demo.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Claves;
import com.example.demo.model.Notas;
import com.example.demo.model.Usuario;
import com.example.demo.repository.NotasRepository;
import com.example.demo.repository.UsuarioRepository;

@RestController
@RequestMapping("V1/notas")

//creaar notas
public class NotasController {
@Autowired
public NotasRepository notasRepository;

@Autowired 
private UsuarioRepository usuarioRepository;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Object> crearClave(@Valid @RequestBody Notas nota) {
		notasRepository.save(nota);
		return new ResponseEntity<Object>(nota, HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Object> listarunaclave() {
		return new ResponseEntity<Object>(notasRepository.findAll(), HttpStatus.OK);
	}
	
//mostrar todas las notas 
	@RequestMapping(method = RequestMethod.GET, path = "/{usuario}")
	public ResponseEntity<Object> listarunanota(@PathVariable(value = "usuario") Integer id) {
		return new ResponseEntity<Object>(notasRepository.findById(id), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.DELETE,path="/{usuarioid}")
		public ResponseEntity<Object> eliminarNota(@PathVariable (value="usuarioid") Integer id){
			notasRepository.deleteById(id);
			return new ResponseEntity<Object>(null, HttpStatus.NO_CONTENT);
		}
	
}

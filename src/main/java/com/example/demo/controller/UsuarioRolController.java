package com.example.demo.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.UsuarioRol;
import com.example.demo.repository.UsuarioRolRepository;

@RestController
@RequestMapping(value="/V1/usuarioitem")
public class UsuarioRolController {

	@Autowired
	
	UsuarioRolRepository usuariorolRepository;
	
	//retorna todos los documentos
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity <Object> listardocumentos(){
		return new ResponseEntity<Object>(usuariorolRepository.findAll(),HttpStatus.OK);
		}
	
	//Retorna un documento
	@RequestMapping(method= RequestMethod.GET, path="/{id}")
	public  ResponseEntity<Object>listarundocumento(@PathVariable("id") Integer id){
	return new ResponseEntity<Object>(usuariorolRepository.findById(id), HttpStatus.OK);	
	}
	
	//Crear y guaradr
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Object> creardocumento(@Valid @RequestBody UsuarioRol usuariorol){
		usuariorolRepository.save(usuariorol);
		return new ResponseEntity<Object>(usuariorol,HttpStatus.CREATED);
	}
	
	//editar
	
	@RequestMapping(method=RequestMethod.PUT,path="/{id}")
	public ResponseEntity<UsuarioRol> actualizarpersona(@PathVariable(value="id") Integer usuarioid, @Valid @RequestBody UsuarioRol detalleusuariorol){
	
	Optional<UsuarioRol> usuario= usuariorolRepository.findById(usuarioid);
	if(!usuario.isPresent())
		return new ResponseEntity<UsuarioRol>(new UsuarioRol(), HttpStatus.NO_CONTENT);
	usuario.get().setDescripcion(detalleusuariorol.getDescripcion());
	usuario.get().setId(detalleusuariorol.getId());
	usuariorolRepository.save(usuario.get());
	
	return new ResponseEntity<UsuarioRol>(usuario.get(), HttpStatus.OK);
	
	
	}
	
	@RequestMapping(method=RequestMethod.DELETE, path="/{id}")
	
	public ResponseEntity<Void> eliminarusuario(@PathVariable("id") Integer id){
	usuariorolRepository.deleteById(id);
	
	return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
}
	


}
	
	
package com.example.demo.controller;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.model.Imagen;
import com.example.demo.model.Usuario;
import com.example.demo.repository.ClaveRepository;
import com.example.demo.repository.ImagenRepository;
import com.example.demo.repository.NotasRepository;
import com.example.demo.repository.UsuarioRepository;


@RestController
@RequestMapping(value="/V1/usuario")
public class UsuarioController {
	@Autowired
	ImagenRepository imagenRepository;

	Usuario user;

	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	private BCryptPasswordEncoder bcryptpasswordencoder;
	
	@Autowired
	 NotasRepository notasRepository;
	
	@Autowired
	ClaveRepository claveRepository;

	//retorna todos los usuarios
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity <Object> listardocumentos(@RequestParam("page")int page, @RequestParam("size") int size){
		Page<Usuario> usuario=usuarioRepository.findAll(PageRequest.of(page, size));
		
		if(usuario==null)
		return new ResponseEntity<Object>(usuario,HttpStatus.NO_CONTENT);

		return new ResponseEntity<Object>(usuario,HttpStatus.OK);
		}
	
	//Retorna un usuario
	@RequestMapping(method= RequestMethod.GET, path="/{id}")
	public  ResponseEntity<Object> listarundocumento(@PathVariable("id") Integer id){
	return new ResponseEntity<Object>(usuarioRepository.findById(id), HttpStatus.OK);	
	}
	
	//Retorna un documento
	@RequestMapping(method= RequestMethod.GET, path="/yo")
	public  ResponseEntity<Object> yo(Authentication authentication){
	Usuario usuario=usuarioRepository.findByLogin(authentication.getName());
	return new ResponseEntity<Object>(usuario, HttpStatus.OK);	
	}
	
	//Crear y guaradr
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Object> crearusuario(@Valid @RequestBody Usuario usuario){
		//aqui se encripta la contraseņa
		usuario.setClave(bcryptpasswordencoder.encode(usuario.getClave()));
		usuarioRepository.save(usuario);

		
		return new ResponseEntity<Object>(usuario,HttpStatus.CREATED);
	}
	
	//editar
	
	@RequestMapping(method=RequestMethod.PUT,path="/{id}")
	public ResponseEntity<Usuario> actualizarpersona(@PathVariable(value="id") Integer usuarioid, @Valid @RequestBody Usuario detalleusuario){
	
	Optional<Usuario> usuario= usuarioRepository.findById(usuarioid);
	if(!usuario.isPresent())
		return new ResponseEntity<Usuario>(new Usuario(), HttpStatus.NO_CONTENT);
	
	usuario.get().setApellidos(detalleusuario.getApellidos());
	usuario.get().setClave(detalleusuario.getClave());
	usuario.get().setEmail(detalleusuario.getEmail());
	usuario.get().setId(detalleusuario.getId());
	usuario.get().setLogin(detalleusuario.getLogin());
	usuario.get().setNombres(detalleusuario.getNombres());
	usuario.get().setRol(detalleusuario.getRol());
	usuario.get().setTelefono(detalleusuario.getTelefono());
	//hasta aqui
	usuarioRepository.save(usuario.get());
	
	return new ResponseEntity<Usuario>(usuario.get(), HttpStatus.OK);
	
	
	}
	
	@RequestMapping(method=RequestMethod.DELETE, path="/{id}")
	
	public ResponseEntity<Void> eliminarusuario(@PathVariable("id") Integer id){
	usuarioRepository.deleteById(id);
	
	return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
}
	
	
	@RequestMapping(method = RequestMethod.GET, path = "/notasporid/{id}")
	public ResponseEntity<Object> listarnotaporid(@PathVariable(value="id")int id) {	
		return new ResponseEntity<Object>(notasRepository.findByUsuarioId(id), HttpStatus.OK);
}

	
	@RequestMapping(method= RequestMethod.GET,path="/clavesporid/{id}")
	public ResponseEntity<Object> listarporclaveid(@PathVariable(value="id") int id){
		return new ResponseEntity<Object> (claveRepository.findByUsuarioId(id), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.GET,path="/verfoto")
	public ResponseEntity<Object>verImagen(@RequestParam(value="id")int id){
		return new ResponseEntity<Object> (imagenRepository.findById(id), HttpStatus.OK);
	}
	
	
	//agrega una imagen,  y crea un directorio nuevo.
	
	@RequestMapping(method=RequestMethod.POST,path="/img")
	public ResponseEntity<Object> guardarImagen(@RequestParam(value="archivo") MultipartFile foto, @RequestParam(value="login") String login) throws IOException {
	 //esto es para obtener el usuario actual supuestamente
		Imagen imagen=new Imagen();

		if (!foto.isEmpty()) {	
			

			//creo el directorio donde se va guardar
			Path directorioRecursos=Paths.get("assets//uploads");
			//obtengo la ruta absoluta
			String rootPath=directorioRecursos.toFile().getAbsolutePath();
			String nombrefoto=foto.getOriginalFilename();
			
			Path rutaCompleta= Paths.get(rootPath+ "//" + Integer.parseInt(login) + "//" + foto.getOriginalFilename());
			String ruta=rutaCompleta.toString();
			System.out.println(ruta);
			//System.out.println(login);
			//obtengo los bytes
			
			try {
				byte[] bytes=foto.getBytes();
				//y ahora la ruta final
				//crea un directorio con el numero del id de usuario
			
			
				Files.createDirectories(Paths.get("assets//uploads//"+ Integer.parseInt(login)));	

				Files.write(rutaCompleta, bytes);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(login);
			Usuario user= usuarioRepository.findByLogin(login);
		    imagen.setUsuario(user);
		    imagen.setId(Integer.parseInt(login));
			imagen.setRuta("assets//uploads//" +login+ "//" +nombrefoto);
			System.out.println(nombrefoto);
		
			imagenRepository.save(imagen);
		
	}
		return new ResponseEntity<Object>(imagen,HttpStatus.CREATED);
		

	}
	
	
	//eliminar foto de la bd y  el directorio de imagenes
	@RequestMapping(value= "/eliminar/{id}")
	public ResponseEntity<Object> borrarFoto(@PathVariable(value="id") int id){
	if (id>0) {
		Optional<Usuario> usuario=usuarioRepository.findById(id);
		Imagen imagen= imagenRepository.getOne(id);
		
		Path ruta=Paths.get(imagen.getRuta()).toAbsolutePath();
	
		File archivo= ruta.toFile();
		
		if(archivo.delete()) {
			System.out.println("sirve");
			imagenRepository.deleteById(id);
		}
		
	}	
	return new ResponseEntity<Object>(HttpStatus.OK);

	
	}
	
}

package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.demo.model.Usuario;
import com.example.demo.repository.UsuarioRepository;

@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService {

	@Autowired
	UsuarioRepository usuarioRepository;

	public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
		Usuario usuario = usuarioRepository.findByLogin(userId);
		if (usuario == null) {
			throw new UsernameNotFoundException("Invalid username or password.");
		}
	
		return new User(usuario.getLogin(), usuario.getClave(),true,true,true,true,getAuthority(usuario));
	}

	private List getAuthority(Usuario usuario) {
		List roles = new ArrayList();
		
		//NO ENTIENDO ADA
		//for (Usuario r : usuario.getEmail()) {
		//	roles.add(new SimpleGrantedAuthority(r.getEmail()));
		//}
		roles.add(new SimpleGrantedAuthority("user"));

		return roles;
	}

	public List findAll() {
		List<Object> list = new ArrayList<>();
		usuarioRepository.findAll().iterator().forEachRemaining(list::add);
		return list;
	}
}

package com.example.demo.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity(name="imagen")

public class Imagen {
@Column(name="id")
@Id
public Integer id;



@Column(name="ruta")
public String ruta;


@OneToOne()
@PrimaryKeyJoinColumn(name="usu_id")
private Usuario usuario;

public Usuario getUsuario() {
	return usuario;
}

public void setUsuario(Usuario usuario) {
	this.usuario = usuario;
}

public Integer getId() {
	return id;
}

public String getRuta() {
	return ruta;
}

public void setId(Integer id) {
	this.id = id;
}

public void setRuta(String ruta) {
	this.ruta = ruta;
}



}

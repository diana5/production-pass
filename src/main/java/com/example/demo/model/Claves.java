package com.example.demo.model;

import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity(name="claves")
public class Claves {
@Column(name="id")
@GeneratedValue(strategy=GenerationType.IDENTITY)
@Id
Integer id;

@Column(name="name")
String name;

@Column(name="apellido")
String apellido;

@Column(name="email")
String email;

@Column(name="fecha")
Date fecha;

@Column(name="notas")
String notas;

@Column(name="clave")
String clave;

@ManyToOne
@JoinColumn(name="usu_id")
private Usuario usuario;

public Usuario getUsuario() {
	return usuario;
}

public void setUsuario(Usuario usuario) {
	this.usuario = usuario;
}

public String getClave() {
	return clave;
}

public void setClave(String clave) {
	this.clave = clave;
}

public Integer getId() {
	return id;
}

public String getName() {
	return name;
}

public String getApellido() {
	return apellido;
}

public String getEmail() {
	return email;
}

public Date getFecha() {
	return fecha;
}

public String getNotas() {
	return notas;
}

public void setId(Integer id) {
	this.id = id;
}

public void setName(String name) {
	this.name = name;
}

public void setApellido(String apellido) {
	this.apellido = apellido;
}

public void setEmail(String email) {
	this.email = email;
}

public void setFecha(Date fecha) {
	this.fecha = fecha;
}

public void setNotas(String notas) {
	this.notas = notas;
}

}
